package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        String humanInput = "";
        boolean validInput = false;
        String computerInput;
        String declareWinner;
        boolean keepPlaying = true;
        String inputKeepPlaying = "";

        while (keepPlaying == true) {
            System.out.println("Let's play round " + roundCounter);
            while (validInput == false) {
                humanInput = readInput("Your choice (Rock/Paper/Scissors)?");
                validInput = validateInput(humanInput.toLowerCase());
                if (validInput == true) {
                    break;
                }
                System.out.println("I do not understand " + humanInput + ". Could you try again?");
            }

            computerInput = computerChoice();
            declareWinner = getWinner(humanInput.toLowerCase(), computerInput.toLowerCase());
            System.out.println("Human chose " + humanInput + ", computer chose " + computerInput + "." + declareWinner);
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            inputKeepPlaying = readInput("Do you wish to continue playing? (y/n)?");
            if(!inputKeepPlaying.equals("y")) {
                System.out.println("Bye bye :)");
                break;
            }
            validInput = false;
            roundCounter += 1;
        }

        
    }

    private String getWinner(String humanInput, String computerInput) {
        if(humanInput.equals(computerInput)) {
            return " It's a tie!";
        }
        else if(humanInput.equals("rock")) {
            if(computerInput.equals("scissors")) {
                humanScore += 1;
                return " Human wins!";
            }
            else {
                computerScore += 1;
                return " Computer wins!";
            }
        }
        else if(humanInput.equals("paper")) {
            if(computerInput.equals("rock")) {
                humanScore += 1;
                return " Human wins!";
            }
            else {
                computerScore += 1;
                return " Computer wins!";
            }
        }
        else {
            if(computerInput.equals("paper")) {
                humanScore += 1;
                return " Human wins!";
            }
            else {
                computerScore += 1;
                return " Computer wins!";
            }
        }
    }

    private boolean validateInput(String humanInput) {
        //if(rpsChoices.contains(humanInput)){}
        if(humanInput.equals("rock") || humanInput.equals("paper") || humanInput.equals("scissors")) {
            return true;
        }
       
        else {
            return false;
        }
    }

    public String computerChoice() {
        String output = "";
        Random rand = new Random();
        int choice = rand.nextInt(3);
        if(choice == 0) {
            output = "rock";
        }
        if(choice == 1) {
            output = "paper";
        }
        if(choice == 2) {
            output = "scissors";
        }
        return output;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
